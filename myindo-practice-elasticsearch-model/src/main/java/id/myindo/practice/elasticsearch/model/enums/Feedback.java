package id.myindo.practice.elasticsearch.model.enums;

/**
 * @author isman
 * @Project myindo-platform
 * @Package id.myindo.platform.account.personal.model.enums
 * @ON 5/29/19
 */
public enum Feedback {
    NONE(0),POSITIVE(1),NETRAL(2),NEGATIVE(3);

    private final int value;

    Feedback(int value) {this.value = value;

    }
    public int getValue() {return value;}

    public static Feedback fromValue(int value) {
        for (Feedback aip: values()) {
            if (aip.getValue() == value) {
                return aip;
            }
        }
        return null;
    }
}
