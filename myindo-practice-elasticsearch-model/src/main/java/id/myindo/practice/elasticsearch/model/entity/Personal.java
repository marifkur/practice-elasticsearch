package id.myindo.practice.elasticsearch.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.myindo.platform.framework.entity.BaseIdEntity;
import id.myindo.platform.framework.enums.Gender;
import id.myindo.platform.framework.enums.MaritalStatus;
import id.myindo.platform.framework.enums.Religion;
import id.myindo.platform.framework.enums.Status;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 6/24/19
 * @Time 11:24 AM
 * @Encoding UTF-8
 * @Project MyIndo Platform
 * @Package
 */

@Entity
@Table(name = "personal",
        uniqueConstraints={
                @UniqueConstraint(columnNames={"code"},name = "UK_Personal_Code")
        },
        indexes = {
                @Index(columnList= "first_name", name = "IDX_Personal_FirstName"),
                @Index(columnList="last_name", name = "IDX_Personal_LastName"),
                @Index(columnList="nickname", name = "IDX_Personal_Nickname"),
                @Index(columnList="gender", name = "IDX_Personal_Gender"),
                @Index(columnList="marital_status", name = "IDX_Personal_MaritalStatus"),
                @Index(columnList="religion", name = "IDX_Personal_Religion"),
                @Index(columnList="birth_date", name = "IDX_Personal_BirthDate"),
                @Index(columnList="mother_name", name = "IDX_Personal_MotherName"),
                @Index(columnList="status", name = "IDX_Personal_Status")
        }
)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Document(indexName = "personal", type = "_doc")
public class Personal extends BaseIdEntity{
    @NotNull(message = "code can't null")
    @NotEmpty
    @Column(name = "code", length = 50, nullable = false)
    private String code;

    @NotNull(message = "name can't null")
    @NotEmpty
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @NotNull(message = "firstName can't null")
    @NotEmpty
    @Column(name = "first_name", length = 50, nullable = false)
    private String firstName;

    @NotNull(message = "middleName can't null")
    @Column(name = "middle_name", length = 50, nullable = false)
    private String middleName;

    @NotNull(message = "lastName can't null")
    @NotEmpty
    @Column(name = "last_name", length = 50, nullable = false)
    private String lastName;

    @NotNull(message = "nickName can't null")
    @NotEmpty
    @Column(name = "nickname", length = 50, nullable = false)
    private String nickName;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "gender",  length = 1)
    @ColumnDefault(value = "")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "marital_status",  length = 1)
    @ColumnDefault(value = "")
    private MaritalStatus maritalStatus;


    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "religion",  length = 20)
    @ColumnDefault(value = "")
    private Religion religion;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date", nullable = false)
    @ColumnDefault(value = "")
    private Date birthDate;

    @Column(name = "place_of_birth", nullable = false)
    @NotEmpty
    @ColumnDefault(value = "")
    private String placeOfBirth;

    @Column(name = "mother_name", length = 50, nullable = false)
    @ColumnDefault(value = "")
    private String motherName;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
