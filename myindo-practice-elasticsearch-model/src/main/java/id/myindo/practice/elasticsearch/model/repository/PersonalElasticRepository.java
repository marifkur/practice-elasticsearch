package id.myindo.practice.elasticsearch.model.repository;

import id.myindo.platform.framework.enums.Status;
import id.myindo.platform.framework.repository.BaseRepository;
import id.myindo.practice.elasticsearch.model.entity.Personal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @Author yogisyaputra <yogi.syaputra@myindo.co.id>
 * @Since 2/4/19
 * @Time 2:47 PM
 * @Encoding UTF-8
 * @Project myindo-platform
 * @Package
 */

@Repository
public interface PersonalElasticRepository extends BaseRepository<Personal, String>, JpaSpecificationExecutor<Personal> {
    Personal findByIdAndStatus(String id, Status status);
    Page<Personal> findByName(String name, PageRequest pageRequest);

    List<Personal> findByNickName(String nickName);
    Personal findByCode(String code);
    List<Personal> findByIdAndStatusNotIn(String id, Collection<Status> statuses);

}
