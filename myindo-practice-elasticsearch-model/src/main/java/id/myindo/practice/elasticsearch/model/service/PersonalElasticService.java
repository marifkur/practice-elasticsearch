package id.myindo.practice.elasticsearch.model.service;

import id.myindo.platform.framework.enums.Status;
import id.myindo.platform.framework.service.BaseService;
import id.myindo.practice.elasticsearch.model.entity.Personal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @Author yogisyaputra <yogi.syaputra@myindo.co.id>
 * @Since 2/4/19
 * @Time 2:49 PM
 * @Encoding UTF-8
 * @Project myindo-platform
 * @Package
 */
public interface PersonalElasticService extends BaseService<Personal> {
    /*Personal save(Personal personal);

    void delete(Personal personal);

    Iterable<Personal> findAll();

    Page<Personal> findByName(String name, PageRequest pageRequest);

    List<Personal> findByNickName(String nickName);*/

    String saveToElastic(Personal personal);
    String updateFromElastic(String id, Personal personal);
    String deleteFromElastic(String id);
    Map<String, Object> viewFromElastic(String id);
    Map<String, Object> searchByNameFromElastic(String name);
    Personal findByCode(String code);
    List<Personal> findByIdAndStatusNotIn(String id, Collection<Status> statuses);

}
