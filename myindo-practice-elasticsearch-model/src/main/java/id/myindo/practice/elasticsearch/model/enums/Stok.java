package id.myindo.practice.elasticsearch.model.enums;

/**
 * @author isman
 * @Project myindo-platform
 * @Package id.myindo.platform.account.personal.model.enums
 * @ON 5/29/19
 */
public enum Stok {
    LIMITED(0),UNLIMITED(1);

    private final int value;

    Stok(int value) {this.value = value;

    }
    public int getValue() {return value;}

    public static Stok fromValue(int value) {
        for (Stok aip: values()) {
            if (aip.getValue() == value) {
                return aip;
            }
        }
        return null;
    }
}
