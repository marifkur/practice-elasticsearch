package id.myindo.practice.elasticsearch.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author Sintong Panjaitan <sintongjait@gmail.com>
 * @Since 3/19/19
 * @Time 2:38 PM
 * @Encoding UTF-8
 * @Project Myindo Platform
 * @Package
 */
@Getter
@AllArgsConstructor
public enum Weight {
    GRAM("GRAM","Gram"),
    KILOGRAM("KILOGRAM","Kilogram");

    private final String value;

    private final String description;


}
