package id.myindo.practice.elasticsearch.model.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 6/24/19
 * @Time 11:46 AM
 * @Encoding UTF-8
 * @Project MyIndo Platform
 * @Package
 */

@Configuration
public class ElasticSearchConfig {

//    @Value("${elasticsearch.host:localhost}")

    public String host = "localhost";

//    @Value("${elasticsearch.port:9300}")

    public int port = 9300;

    public String getHost() {

        return host;

    }

    public int getPort() {

        return port;

    }

    @Bean
    public Client client(){

        TransportClient client = null;

        try{

            client = new PreBuiltTransportClient(
                    Settings.builder()
                            .put("cluster.name","elasticsearch").build())
                    .addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"), 9300));

        } catch (UnknownHostException e) {

            e.printStackTrace();

        }

        return client;

    }
}
