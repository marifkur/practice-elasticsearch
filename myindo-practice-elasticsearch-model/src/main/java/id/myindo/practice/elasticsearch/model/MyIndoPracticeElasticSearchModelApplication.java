package id.myindo.practice.elasticsearch.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
		scanBasePackages = {
				"id.myindo.practice.elasticsearch.model",
				"id.myindo.platform.framework",
				"id.myindo.platform.security.utils",
				"id.myindo.platform.redis.service",
				"id.myindo.platform.redis",
				"id.myindo.platform.redis.utils",
				"id.myindo.platform.redis",

		}
)

@ComponentScan(
		basePackages = {
				"id.myindo.practice.elasticsearch.model.service",
				"id.myindo.practice.elasticsearch.model.config",
//				"id.myindo.practice.elasticsearch.model.enums",
				"id.myindo.platform.framework.exception",
				"id.myindo.platform.framework.enums",
				"id.myindo.platform.framework.service",
				"id.myindo.platform.security.utils.core",
				"id.myindo.platform.security.utils.crypto",
				"id.myindo.platform.security.utils.password",
				"id.myindo.platform.security.utils.utils",
				"id.myindo.platform.security.utils",
				"id.myindo.platform.redis.service",
				"id.myindo.platform.redis.config",
				"id.myindo.platform.redis.utils",
				"id.myindo.platform.redis.service",
				"id.myindo.platform.redis.utils",
				"id.myindo.platform.redis.config",

		}
)

@EnableJpaRepositories(
		basePackages = {
				"id.myindo.practice.elasticsearch.model.repository",
				"id.myindo.platform.framework.repository",
		}
)

@EntityScan(
		basePackages = {
				"id.myindo.practice.elasticsearch.model",
				"id.myindo.platform.framework",
				"id.myindo.platform.redis",
		}
)
public class MyIndoPracticeElasticSearchModelApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyIndoPracticeElasticSearchModelApplication.class, args);
	}

}
