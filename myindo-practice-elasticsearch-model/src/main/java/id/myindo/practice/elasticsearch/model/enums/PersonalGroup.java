package id.myindo.practice.elasticsearch.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author Sintong Panjaitan <sintongjait@gmail.com>
 * @Since 3/19/19
 * @Time 2:38 PM
 * @Encoding UTF-8
 * @Project Myindo Platform
 * @Package
 */
@Getter
@AllArgsConstructor
public enum PersonalGroup {
    IDENTITY("IDENTITY","Identitas Pribadi"),
    CARD("CARD","Kartu Pribadi"),
    DETAIL("DETAIL","Detail Pribadi"),
    FAMILY("FAMILY","Keluarga"),
    ADDRESS("ADDRESS","Alamat"),
    EDUCATION("EDUCATION","Pendidikan"),
    SKILL("SKILL","Kemampuan"),
    JOBEXPERIENCE("JOBEXPERIENCE","Pengalaman Kerja"),
    ORGANISATION("ORGANISATION","Organisasi");

    private final String value;

    private final String description;


}
