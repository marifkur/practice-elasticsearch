package id.myindo.practice.elasticsearch.model.service;

import id.myindo.platform.framework.enums.Status;
import id.myindo.practice.elasticsearch.model.entity.Personal;
import id.myindo.practice.elasticsearch.model.repository.PersonalElasticRepository;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * @Author yogisyaputra <yogi.syaputra@myindo.co.id>
 * @Since 2/4/19
 * @Time 2:49 PM
 * @Encoding UTF-8
 * @Project myindo-platform
 * @Package
 */

@Service("personalElasticService")
public class PersonalElasticServiceImpl implements PersonalElasticService {

    private static Logger logger = LoggerFactory.getLogger(PersonalElasticServiceImpl.class);

    @Autowired
    public PersonalElasticRepository repository;

    @Autowired
    Client client;

    @Override
    public String saveToElastic(Personal personal) {
        IndexResponse response = null;
        try {
            response = client.prepareIndex("personal", "_doc", personal.getId())
                    .setSource(jsonBuilder()
                            .startObject()
                            .field("name", personal.getName())
                            .field("firstName", personal.getFirstName())
                            .field("middleName", personal.getMiddleName())
                            .field("lastName", personal.getLastName())
                            .field("nickName", personal.getNickName())
                            .field("gender", personal.getGender())
                            .field("maritalStatus", personal.getMaritalStatus())
                            .field("religion", personal.getReligion())
                            .field("birthDate", personal.getBirthDate())
                            .field("placeOfBirth", personal.getPlaceOfBirth())
                            .field("motherName", personal.getMotherName())
                            .endObject()
                    )
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("response id:" + response.getId());

        return response.getResult().toString();
    }

    @Override
    public String updateFromElastic(String id, Personal personal) {


        try {
            UpdateRequest updateRequest = new UpdateRequest();

            updateRequest.index("personal")

                    .type("_doc")

                    .id(id)

                    .doc(jsonBuilder()
                            .startObject()
                            .field("name", personal.getName())
                            .field("firstName", personal.getFirstName())
                            .field("middleName", personal.getMiddleName())
                            .field("lastName", personal.getLastName())
                            .field("nickName", personal.getNickName())
                            .field("gender", personal.getGender())
                            .field("maritalStatus", personal.getMaritalStatus())
                            .field("religion", personal.getReligion())
                            .field("birthDate", personal.getBirthDate())
                            .field("placeOfBirth", personal.getPlaceOfBirth())
                            .field("motherName", personal.getMotherName())
                            .endObject());
            UpdateResponse updateResponse = client.update(updateRequest).get();

            System.out.println(updateResponse.status());

            return updateResponse.status().toString();

        } catch (InterruptedException | ExecutionException e) {

            System.out.println(e);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Exception";
    }

    @Override
    public String deleteFromElastic(String id) {
        DeleteResponse deleteResponse = client.prepareDelete("personal", "_doc", id).get();
        DeleteRequest deleteRequest = client.prepareDelete().request();
        client.delete(deleteRequest);


        return deleteResponse.getResult().toString();
    }

    @Override
    public Map<String, Object> viewFromElastic(String id) {
        GetResponse getResponse = client.prepareGet("personal", "_doc", id).get();

        return getResponse.getSource();
    }

    @Override
    public Map<String, Object> searchByNameFromElastic(String name) {
        Map<String, Object> map = null;

        SearchResponse response = client.prepareSearch("personal")

                .setTypes("_doc")
                .setSearchType(SearchType.QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.matchQuery("name", name)).get();

        List<SearchHit> searchHits = Arrays.asList(response.getHits().getHits());

        map = searchHits.get(0).getSourceAsMap();

        return map;
    }

    @Override
    public Personal findByCode(String code) {
        return repository.findByCode(code);
    }

    @Override
    public List<Personal> findByIdAndStatusNotIn(String id, Collection<Status> statuses) {
        return repository.findByIdAndStatusNotIn(id, statuses);
    }

    @Override
    @Transactional
    public Personal save(Personal entity) {
        Personal personalResponse = null;

        if (entity.getId() != null) {
            Personal personalValid = null;

            try {
                personalValid = repository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            } catch (Exception e) {
                return null;
            }

            if (personalValid != null) {
                personalValid.setName(entity.getName());
                personalValid.setFirstName(entity.getFirstName());
                personalValid.setMiddleName(entity.getMiddleName());
                personalValid.setLastName(entity.getLastName());
                personalValid.setNickName(entity.getNickName());
                personalValid.setGender(entity.getGender());
                personalValid.setMaritalStatus(entity.getMaritalStatus());
                personalValid.setReligion(entity.getReligion());
                personalValid.setBirthDate(entity.getBirthDate());
                personalValid.setPlaceOfBirth(entity.getPlaceOfBirth());
                personalValid.setMotherName(entity.getMotherName());
                personalValid.setStatus(entity.getStatus());

                Personal updatePersonal = repository.save(personalValid);
                personalResponse = updatePersonal;
            } else {
                return null;
            }
        } else {
            personalResponse = repository.save(entity);
        }

        return personalResponse;
    }

    @Override
    public Boolean delete(Personal entity) {
        boolean response = false;
        if (entity.getId() != null) {
            Personal validPersonal = repository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonal != null) {
                validPersonal.setStatus(Status.DELETE);
                repository.save(validPersonal);
                response = true;
            }
        }


        return response;
    }

    @Override
    public Personal findById(String id) {
        Personal validPersonal = null;
        try {
            validPersonal = repository.findByIdAndStatus(id, Status.ACTIVE);
        } catch (Exception ex) {
            return null;
        }


        return validPersonal;
    }

    @Override
    public Page<Personal> findAllPage(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
