package id.myindo.practice.elasticsearch.model.service;

import id.myindo.platform.framework.enums.Gender;
import id.myindo.platform.framework.enums.MaritalStatus;
import id.myindo.platform.framework.enums.Religion;
import id.myindo.platform.framework.enums.Status;
import id.myindo.practice.elasticsearch.model.entity.Personal;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 6/26/19
 * @Time 1:43 PM
 * @Encoding UTF-8
 * @Project MyIndo Platform
 * @Package
 */

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonalElasticServiceTest {

    @Autowired
    @Qualifier("personalElasticService")
    private PersonalElasticService personalElasticService;

    static String id;

    private static Logger logger = LoggerFactory.getLogger(PersonalElasticServiceTest.class);

    @Test
    @Rollback(false)
    public void personal01SaveToElastic() throws Exception {
        Personal personal = new Personal();

        Gender gender = Gender.M;
        MaritalStatus maritalStatus = MaritalStatus.M;
        Religion religion = Religion.ISLAM;

        personal.setCode("ble");
        personal.setName("Test");
        personal.setFirstName("Test");
        personal.setMiddleName("Test");
        personal.setLastName("Test");
        personal.setNickName("Test");
        personal.setGender(gender);
        personal.setMaritalStatus(maritalStatus);
        personal.setReligion(religion);
        personal.setBirthDate(new Date());
        personal.setPlaceOfBirth("test");
        personal.setMotherName("test");
        personal.setStatus(Status.ACTIVE);
        personal.setCreateBy("system");
        personal.setModifiedBy("system");
        personal.setModifiedAt(LocalDateTime.now());
        personal.setCreateIp("172.17.3.1");
        personal.setModifiedIp("172.17.3.1");


        Personal validPersonal = personalElasticService.save(personal);
        id = validPersonal.getId();
        personalElasticService.saveToElastic(validPersonal);
        logger.info("------------------------ID------------------------"+id);
    }

    @Test
    @Rollback(false)
    public void personal02UpdateFromElastic() throws Exception {
        Personal personal1 = personalElasticService.findById(id);

        Gender gender = Gender.M;
        MaritalStatus maritalStatus = MaritalStatus.M;
        Religion religion = Religion.ISLAM;

        Personal personal = new Personal();
        personal.setId(personal1.getId());
        personal.setCode("ble");
        personal.setName("Test");
        personal.setFirstName("Test");
        personal.setMiddleName("Test");
        personal.setLastName("Test");
        personal.setNickName("Test");
        personal.setGender(gender);
        personal.setMaritalStatus(maritalStatus);
        personal.setReligion(religion);
        personal.setBirthDate(new Date());
        personal.setPlaceOfBirth("test");
        personal.setMotherName("test");
        personal.setStatus(Status.ACTIVE);
        personal.setCreateBy("system");
        personal.setModifiedBy("system");
        personal.setModifiedAt(LocalDateTime.now());
        personal.setCreateIp("172.17.3.1");
        personal.setModifiedIp("172.17.3.1");


        Personal validPersonal = personalElasticService.save(personal);

        personalElasticService.UpdateFromElastic(id, validPersonal);
        logger.info(id);

    }
    @Test
    @Rollback(false)
    public void personal03ViewFromElastic() throws Exception {
        Map<String, Object> map = personalElasticService.viewFromElastic(id);

    }

    @Test
    @Rollback(false)
    public void personal04SearchByNameFromElastic() throws Exception {
        Map<String, Object> map = personalElasticService.searchByNameFromElastic("arif");
    }

    @Test
    @Rollback(false)
    public void personal05DeleteFromElastic() throws Exception {
        Personal personal = personalElasticService.findById(id);

        String del = personalElasticService.deleteFromElastic(personal.getId());
        personalElasticService.delete(personal);
    }



}