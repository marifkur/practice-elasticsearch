package id.myindo.practice.elasticsearch.api.controller;

import id.myindo.platform.framework.enums.Status;
import id.myindo.platform.framework.exception.*;
import id.myindo.platform.framework.json.Error;
import id.myindo.platform.framework.json.JsonResponse;
import id.myindo.platform.framework.json.Source;
import id.myindo.platform.security.server.resource.model.CustomPrincipal;
import id.myindo.platform.security.utils.core.MySecurityUtils;
import id.myindo.practice.elasticsearch.api.form.PersonalAddForm;
import id.myindo.practice.elasticsearch.api.form.PersonalForm;
import id.myindo.practice.elasticsearch.api.model.PersonalListResponse;
import id.myindo.practice.elasticsearch.model.entity.Personal;
import id.myindo.practice.elasticsearch.model.service.PersonalElasticService;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * @Author yogisyaputra <yogi.syaputra@myindo.co.id>
 * @Since 2/7/19
 * @Time 3:02 PM
 * @Encoding UTF-8
 * @Project myindo-platform
 * @Package
 */
@RestController
@RequestMapping("/elastic")
public class PersonalController {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger(PersonalController.class);

    @Autowired
    public PersonalElasticService personalElasticService;

    @Autowired
    Client client;

    @Autowired
    private MySecurityUtils mySecurityUtils;

    @Value("${myindo.platform.redis.enable:false}")
    private Boolean redisEnable;

    @RequestMapping(value = "/personal/add", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> personalAddPost(CustomPrincipal principal, WebRequest request, HttpServletRequest httpServletRequest, @Valid @RequestBody PersonalForm personalForm, Errors errors) throws SaveException {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Personal personal = new Personal();


        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());
                Error error = new Error();
                error.setCode("1500");
                error.setSource(source);
                error.setDetail("Validation Failed");
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
                response.getErrors().add(error);

            });
            data.put("personal", personalForm);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        Personal validCode = null;
        Personal personalSave = null;
        personalForm.setStatus(Status.ACTIVE);

        String prefix = "123456";
        String gender = personalForm.getGender().toString();

        String code = mySecurityUtils.generateLuhn(prefix, personalForm.getBirthDate(), gender);

        personal.setCode(code);

        try {
            validCode = personalElasticService.findByCode(personal.getCode());
            if (validCode != null) {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(personal.getCode());
                Error error = new Error();
                error.setCode("1502");
                error.setSource(source);
                error.setTitle("Personal Not Found");
                error.setDetail("Personal Not Found");
                error.setStatus(HttpStatus.NOT_FOUND.toString());
                response.getErrors().add(error);
                return new ResponseEntity(response, HttpStatus.NOT_FOUND);
            } else {
                try {
                    personal.setName(personalForm.getName());
                    personal.setFirstName(personalForm.getFirstName());
                    personal.setMiddleName(personalForm.getMiddleName());
                    personal.setLastName(personalForm.getLastName());
                    personal.setNickName(personalForm.getNickName());
                    personal.setGender(personalForm.getGender());
                    personal.setMaritalStatus(personalForm.getMaritalStatus());
                    personal.setReligion(personalForm.getReligion());
                    personal.setBirthDate(personalForm.getBirthDate());
                    personal.setPlaceOfBirth(personalForm.getPlaceOfBirth());
                    personal.setMotherName(personalForm.getMotherName());
                    personal.setStatus(personalForm.getStatus());
                    personal.setCreateBy(principal.getUsername());
                    personal.setModifiedBy(principal.getUsername());
                    personal.setCreateIp(httpServletRequest.getRemoteAddr());
                    personal.setModifiedIp(httpServletRequest.getRemoteAddr());

                    personalSave = personalElasticService.save(personal);
                    data.put("personal", personalSave);
                    personalElasticService.saveToElastic(personalSave);
                    response.setData(data);
                } catch (Exception e) {
                    Source source = new Source();
                    source.setPointer(request.getDescription(false));
                    source.setParameter(new String());
                    Error error = new Error();
                    error.setCode("5000");
                    error.setTitle("SOMETHING ERROR");
                    error.setDetail("Something Error, please contact developer");
                    error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());

                    data.put("personal", personal);
                    response.setData(data);
                    response.getErrors().add(error);
                }
            }
        } catch (Exception e) {
            Error error = new Error();
            error.setCode("5000");
            error.setTitle("SOMETHING ERROR");
            error.setDetail("Something Error, please contact developer");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());

            throw new SaveException(new String(), error, personalForm);
        }

        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/personal/detail", method = RequestMethod.GET)
    public ResponseEntity<JsonResponse<PersonalListResponse>> personalDetailIdGet(CustomPrincipal principal, WebRequest request, @RequestParam("id") String id) throws GetDataException {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Personal personalDetail = null;
        Map<String, Object> dictionaries = new HashMap();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();

        try {
            personalDetail = personalElasticService.findById(id);
            if (personalDetail == null) {
                Error error = new Error();
                error.setCode("1502");
                error.setTitle("Personal Not Found");
                error.setDetail("Personal Not Found");
                error.setStatus(HttpStatus.NOT_FOUND.toString());
                throw new DataNotFoundException(id, error, personalDetail);
            }

            data.put("personalDetail", personalDetail);
            dictionaries.put("id", personalDetail.getId());
            params.put("params", dictionaries);
            response.setData(data);
            response.setDictionaries(params);
        } catch (Exception ex) {
            throw new GetDataException(ex.getLocalizedMessage());
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GetMapping("/personal/view/name/{field}")
    public Map<String, Object> searchByName(@PathVariable final String field) {

       return personalElasticService.searchByNameFromElastic(field);

    }

    @RequestMapping(value = "/personal/update", method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> personalUpdateIdPut(
            CustomPrincipal principal, WebRequest request, HttpServletRequest httpServletRequest, @Valid @RequestBody PersonalForm personalForm, Errors errors,
            @RequestParam("id") String id) throws UpdateException {

        personalForm.setStatus(Status.ACTIVE);
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Personal personalValid = null;
        Map<String, Object> dictionaries = new HashMap();
        Personal personalSave = null;

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(ex -> {
                Source source = new Source();
                source.setPointer(request.getDescription(false));
                source.setParameter(new String());
                Error error = new Error();
                error.setCode("1500");
                error.setSource(source);
                error.setDetail("Validation Failed");
                error.setTitle("Validation Failed");
                error.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
                response.getErrors().add(error);
            });
            data.put("personal", personalForm);
            response.setData(data);
            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            personalValid = personalElasticService.findById(id);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("5000");
            error.setTitle("SOMETHING ERROR");
            error.setDetail("Something Error, please contact developer");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());

            throw new UpdateException(id, error, personalForm);
        }

        if (personalValid == null) {
            Error error = new Error();
            error.setCode("1502");
            error.setTitle("Personal Not Found");
            error.setDetail("Personal Not Found");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            response.getErrors().add(error);

            throw new DataNotFoundException(id, error, personalForm);
        }

        try {

            personalValid.setId(id);
            personalValid.setName(personalForm.getName());
            personalValid.setFirstName(personalForm.getFirstName());
            personalValid.setMiddleName(personalForm.getMiddleName());
            personalValid.setLastName(personalForm.getLastName());
            personalValid.setNickName(personalForm.getNickName());
            personalValid.setGender(personalForm.getGender());
            personalValid.setMaritalStatus(personalForm.getMaritalStatus());
            personalValid.setReligion(personalForm.getReligion());
            personalValid.setBirthDate(personalForm.getBirthDate());
            personalValid.setPlaceOfBirth(personalForm.getPlaceOfBirth());
            personalValid.setMotherName(personalForm.getMotherName());
            personalValid.setStatus(personalForm.getStatus());

            personalValid.setCreateBy(principal.getUsername());
            personalValid.setModifiedBy(principal.getUsername());
            personalValid.setModifiedIp(httpServletRequest.getRemoteAddr());

            personalSave = personalElasticService.save(personalValid);
            personalElasticService.updateFromElastic(id, personalSave);
            dictionaries.put("id", personalValid.getId());
            data.put("personal", personalSave);
            params.put("params", dictionaries);
            response.setData(data);
            response.setDictionaries(params);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("5000");
            error.setTitle("SOMETHING ERROR");
            error.setDetail("Something Error, please contact developer");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());

            throw new UpdateException(personalValid.getId(), error, personalValid);
        }
        return new ResponseEntity(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/personal/delete", method = RequestMethod.DELETE)
    public ResponseEntity<?> personalDeleteIdDelete(CustomPrincipal principal, WebRequest request, HttpServletRequest httpServletRequest, @RequestParam("id") String id) throws DeleteException {

        JsonResponse<Map<String, Object>> response = new JsonResponse();

        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();
        Personal personal = null;

        try {
            personal = personalElasticService.findById(id);
        } catch (Exception ex) {
            Error error = new Error();
            error.setCode("5000");
            error.setTitle("SOMETHING ERROR");
            error.setDetail("Something Error, please contact developer");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());

            throw new DeleteException(id, error, personal);
        }

        if (personal != null) {
            try {

                personal.setCreateBy(principal.getUsername());
                personal.setModifiedBy(principal.getUsername());
                personal.setModifiedIp(httpServletRequest.getRemoteAddr());

                personalElasticService.delete(personal);
                personalElasticService.deleteFromElastic(id);
                dictionaries.put("id", personal.getId());
                data.put("personal", personal);
                params.put("params", dictionaries);
                response.setData(data);
                response.setDictionaries(params);
            } catch (Exception ex) {
                Error error = new Error();
                error.setCode("5000");
                error.setTitle("SOMETHING ERROR");
                error.setDetail("Something Error, please contact developer");
                error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());

                throw new DeleteException(id, error, personal);
            }
        } else {
            Error error = new Error();
            error.setCode("1502");
            error.setTitle("Personal Not Found");
            error.setDetail("Personal Not Found");
            error.setStatus(HttpStatus.NOT_FOUND.toString());

            throw new DataNotFoundException(id, error, personal);
        }

        return new ResponseEntity(response, HttpStatus.OK);

    }

}