package id.myindo.practice.elasticsearch.api.exception;

import id.myindo.platform.framework.exception.MyCustomResponseEntityExceptionHandler;
import id.myindo.platform.framework.json.Error;
import id.myindo.platform.framework.json.JsonResponse;
import id.myindo.platform.framework.json.Source;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
@RestController
public class PersonalApiExceptionHandler extends MyCustomResponseEntityExceptionHandler{

    @ExceptionHandler(value = AccessDeniedException.class)
    public final ResponseEntity<Object> handleAllException(AccessDeniedException ex, WebRequest request){

        JsonResponse<Object> errorResponse = new JsonResponse();

        Source source = new Source();
        source.setParameter("");
        source.setPointer(request.getDescription(false));

        Error error = new Error();
        error.setCode("4001");
        error.setTitle("Access Denied");
        error.setDetail("You have not persmission to access this resources");
        error.setSource(source);

        errorResponse.setData(new ArrayList());

        errorResponse.getErrors().add(error);

        Map<String, Object> params = new LinkedHashMap<>();
        params.put("params",request.getParameterMap());
        errorResponse.setDictionaries(params);

        ex.printStackTrace();

        return new ResponseEntity(errorResponse, HttpStatus.UNAUTHORIZED);
    }

}
