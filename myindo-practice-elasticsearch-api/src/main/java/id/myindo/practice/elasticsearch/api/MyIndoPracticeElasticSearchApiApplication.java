package id.myindo.practice.elasticsearch.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
		scanBasePackages = {
				"id.myindo.platform.framework",
				"id.myindo.platform.redis",
				"id.myindo.platform.security.server.resource",
				"id.myindo.platform.security.utils",
				"id.myindo.practice.elasticsearch.model",
				"id.myindo.practice.elasticsearch.api",
				"id.myindo.platform.account.user.model",
				"id.myindo.platform.account.personal.model",
				"id.myindo.platform.organization.organization.model",
				"id.myindo.platform.content.file.model",

		}
)

@ComponentScan(
		basePackages = {

				"id.myindo.platform.framework.exception",
				"id.myindo.platform.framework.enums",
				"id.myindo.platform.framework.service",

				"id.myindo.practice.elasticsearch.model.service",
				"id.myindo.practice.elasticsearch.model.specification",
				"id.myindo.practice.elasticsearch.model.entity",
				"id.myindo.practice.elasticsearch.model.enums",
				"id.myindo.practice.elasticsearch.model.config",

				"id.myindo.practice.elasticsearch.api.config",
				"id.myindo.practice.elasticsearch.api.controller",
				"id.myindo.practice.elasticsearch.api.exception",
				"id.myindo.practice.elasticsearch.api.form",

				"id.myindo.platform.security.server.resource.config",
				"id.myindo.platform.security.server.resource.model",
				"id.myindo.platform.security.server.resource.utils",
				"id.myindo.platform.security.server.resource.service",

				"id.myindo.platform.security.utils.core",
				"id.myindo.platform.security.utils.crypto",
				"id.myindo.platform.security.utils.password",
				"id.myindo.platform.security.utils.utils",

				"id.myindo.platform.redis.config",
				"id.myindo.platform.redis.service",

				"id.myindo.platform.organization.organization.model.model",
				"id.myindo.platform.organization.organization.model.spesification",
				"id.myindo.platform.organization.organization.model.utils",
				"id.myindo.platform.organization.organization.model.service",
				"id.myindo.platform.organization.organization.model.entity",
				"id.myindo.platform.organization.organization.model.enums",
				"id.myindo.platform.organization.organization.model.payload",
				"id.myindo.platform.organization.organization.model.property",

				"id.myindo.platform.content.file.model.service",
				"id.myindo.platform.content.file.model.entity",
				"id.myindo.platform.content.file.model.myenum",

				"id.myindo.platform.account.user.model.service",
				"id.myindo.platform.account.personal.model.service",
				"id.myindo.platform.account.personal.model.payload",
				"id.myindo.platform.account.personal.model.property",

		}
)

@EnableJpaRepositories(
		basePackages = {
				"id.myindo.practice.elasticsearch.model.repository",
				"id.myindo.platform.content.file.model.repository",
				"id.myindo.platform.framework.repository",
				"id.myindo.platform.account.user.model.repository",
				"id.myindo.platform.account.personal.model.repository",
				"id.myindo.platform.organization.organization.model.repository",

		}
)

@EntityScan(
		basePackages = {
				"id.myindo.practice.elasticsearch.model",
				"id.myindo.platform.content.file.model",
				"id.myindo.platform.framework",
				"id.myindo.platform.security.utils",
				"id.myindo.platform.account.user.model",
				"id.myindo.platform.account.personal.model",
				"id.myindo.platform.organization.organization.model",

		}
)
public class MyIndoPracticeElasticSearchApiApplication extends SpringBootServletInitializer {

	private static Class<MyIndoPracticeElasticSearchApiApplication> application = MyIndoPracticeElasticSearchApiApplication.class;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder) {
		return applicationBuilder.sources(application);
	}

	public static void main(String[] args) {
		SpringApplication.run(MyIndoPracticeElasticSearchApiApplication.class, args);
	}

}
