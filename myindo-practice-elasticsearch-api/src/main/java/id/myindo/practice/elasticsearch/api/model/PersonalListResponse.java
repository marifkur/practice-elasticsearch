package id.myindo.practice.elasticsearch.api.model;

import id.myindo.practice.elasticsearch.model.entity.Personal;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author yogisyaputra <yogi.syaputra@myindo.co.id>
 * @Since 2/7/19
 * @Time 3:05 PM
 * @Encoding UTF-8
 * @Project myindo-platform
 * @Package
 */
@Getter
public class PersonalListResponse {
    List<Personal> personalList = new ArrayList();
}
