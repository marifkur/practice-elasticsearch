//package id.myindo.practice.elasticsearch.api.config;
//
//import id.myindo.platform.security.server.resource.config.MyResourceServerConfiguration;
//import id.myindo.platform.security.server.resource.model.EnableMyResourceServer;
//
///**
// * @Author dwiaribowo <dwi.aribowo@myindo.co.id>
// * @Since 6/18/19
// * @Time 3:37 PM
// * @Encoding UTF-8
// * @Project MyIndo Platform
// * @Package
// */
//@EnableMyResourceServer(resourceIds = "mw/adm")
//public class MyElasticsearchResourceServerConfig extends MyResourceServerConfiguration{
//    public MyElasticsearchResourceServerConfig() {
//        super(MyElasticsearchResourceServerConfig.class);
//    }
//}
