package id.myindo.practice.elasticsearch.api.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import id.myindo.platform.framework.enums.Gender;
import id.myindo.platform.framework.enums.MaritalStatus;
import id.myindo.platform.framework.enums.Religion;
import id.myindo.platform.framework.enums.Status;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author yogisyaputra <yogi.syaputra@myindo.co.id>
 * @Since 2/20/19
 * @Time 2:05 PM
 * @Encoding UTF-8
 * @Project myindo-platform
 * @Package
 */

@Getter
@Setter
public class PersonalForm {

    private String id;

    @NotNull(message = "name can't null")
    @NotEmpty
    private String name;

    @NotNull(message = "firstName can't null")
    @NotEmpty
    private String firstName;

    @NotNull(message = "middleName can't null")
    private String middleName;

    @NotNull(message = "lastName can't null")
    @NotEmpty
    private String lastName;

    @NotNull(message = "nickName can't null")
    @NotEmpty
    private String nickName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private MaritalStatus maritalStatus;

    @Enumerated(EnumType.STRING)
    private Religion religion;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date", nullable = false)
    @ColumnDefault(value = "")
    private Date birthDate;

    private String placeOfBirth;

    @NotNull(message = "motherName can't null")
    private String motherName;

    private Status status;

//    private PersonalPersonalReferrer personalReferrer;
}
